import './App.css';
import { AppRoutes } from '../api';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Home } from '../components/Home';
import { Post, User } from '../components';
import { UsersList } from '../components/UsersList';
import { SignIn, SignUp } from '../components/SignComponents';
import { PostsList } from '../components/PostsList';

const verifyToken = () => {
  console.log('verifying token....');
  const authorize = () => {
    localStorage.clear();
    window.location.reload();
  };

  const token = localStorage.getItem('jwtToken');
  if (token === 'undefined') {
    console.log('Token is invalid!!!');
    authorize();
  }
  if (token && token !== 'undefined') {
    const decodedToken = JSON.parse(window.atob(token.split('.')[1]));
    if (Date.now() > decodedToken.exp * 1000) {
      console.log('Token is invalid!!!');
      authorize();
    } else {
      console.log('Token is OK');
    }
  } else {
    console.log('No token was provided');
  }
};

const signOut = () => {
  localStorage.clear();
  window.location.reload();
};

export function App() {
  verifyToken();
  return (
    <div className='App'>
      <header className='App-header'>
        <div className='logo'>
          <a href='/'>
            <h1>Балаблог</h1>
          </a>
        </div>
        <div className='nav-bar'>
          <div>
            <a href='/users'>Пользователи</a>
          </div>
          <div>
            <a href='/posts'>Посты</a>
          </div>
        </div>
        {localStorage.getItem('jwtToken') ? (
          <div className='log'>
            <a onClick={signOut} href='#'>
              Выйти
            </a>
          </div>
        ) : (
          <div className='log'>
            <a href='/sign-up' className='sign-up'>
              Зарегистрироваться
            </a>
            <a href='/sign-in' className='sign-in'>
              Войти
            </a>
          </div>
        )}
      </header>
      <BrowserRouter>
        <Routes>
          <Route path={AppRoutes.HOME} element={<Home />} />
          <Route path={AppRoutes.USERS_BASE} element={<UsersList />} />
          <Route path={AppRoutes.USERS_BASE + `/:id`} element={<User />} />
          <Route path={AppRoutes.SIGN_IN} element={<SignIn />} />
          <Route path={AppRoutes.SIGN_UP} element={<SignUp />} />
          <Route path={AppRoutes.POSTS_BASE} element={<PostsList />} />
          <Route path={AppRoutes.POSTS_BASE + '/:id'} element={<Post />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
