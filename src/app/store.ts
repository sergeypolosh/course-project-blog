import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { commentsReducer } from '../components/Comment/commentsSlice';
import { postsReducer } from '../components/Post/postsSlice';
import { usersReducer } from '../components/User/usersSlice';

export const store = configureStore({
  reducer: {
    comments: commentsReducer,
    posts: postsReducer,
    users: usersReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
