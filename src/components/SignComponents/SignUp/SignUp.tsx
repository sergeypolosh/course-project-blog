import React from 'react';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../app/store';
import { signUpDto } from '../../../types/types';
import { signUp } from '../../User/usersSlice';
import '../Sign.css';

const toggleClass = (elem: HTMLElement, className: string) => {
  elem.className = elem.className.includes(className)
    ? elem.className.replace(className, '')
    : elem.className.concat(' ' + className);
};

export function SignUp() {
  const dispatch = useDispatch<AppDispatch>();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const formData = new FormData(event.target as HTMLFormElement);
    const formProps = Object.fromEntries(formData) as signUpDto;

    const result = await dispatch(signUp(formProps)).unwrap();

    if (result.statusCode && result.statusCode === 403) {
      const div = document.getElementById('error-messages') as HTMLElement;
      const input = document.getElementById('username') as HTMLInputElement;

      if (div) {
        div.innerText = result.message;
        toggleClass(input, 'error');
        setTimeout(() => {
          div.innerText = '';
          toggleClass(input, 'error');
        }, 3000);
      }
    } else {
      localStorage.setItem('jwtToken', result.jwtToken);
      localStorage.setItem('userId', result.user.userId.toString());
      localStorage.setItem('username', result.user.username);
      window.location.assign('/');
    }
  };

  return (
    <div className='Sign'>
      <form className='input-form' onSubmit={handleSubmit}>
        <label className='required'>Имя пользователя</label>
        <input
          placeholder='Имя пользователя'
          name='username'
          id='username'
          required
          maxLength={30}
        />
        <label className='required'>Пароль</label>
        <input
          placeholder='Пароль'
          type='password'
          name='password'
          required
          maxLength={30}
        />
        <label>Имя</label>
        <input placeholder='Имя' name='firstName' maxLength={30} />
        <label>Фамилия</label>
        <input placeholder='Фамилия' name='lastName' maxLength={30} />
        <input type='submit' className='submit' value={'Зарегистрироваться!'} />
      </form>
      <div id='error-messages'></div>
    </div>
  );
}
