import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { AppDispatch, RootState } from '../../app/store';
import { updateUserDto, UserEntity } from '../../types/types';
import {
  deleteComment,
  selectUserComments,
  selectUserPostsComments,
} from '../Comment/commentsSlice';
import { deletePost, selectUserPosts } from '../Post/postsSlice';
import { UserPostsList } from '../UserPostsList';
import './User.css';
import { deleteUser, selectUserById, updateUser } from './usersSlice';

const toggleClass = (elem: HTMLElement, className: string) => {
  elem.className = elem.className.includes(className)
    ? elem.className.replace(className, '')
    : elem.className.concat(' ' + className);
};

export function User() {
  const { id } = useParams();
  const userId = parseInt(id as string);

  const user = useSelector((state: RootState) => selectUserById(state, userId));
  const posts = useSelector((state: RootState) =>
    selectUserPosts(state, userId)
  );

  const useComments = () => {
    const res = useSelector((state: RootState) =>
      selectUserComments(state, userId)
    );

    for (const postComment of useSelector((state: RootState) =>
      selectUserPostsComments(state, posts, userId)
    )) {
      let shouldAdd = true;
      for (const userComment of res) {
        if (postComment.authorId === userComment.authorId) {
          shouldAdd = false;
        }
      }
      if (shouldAdd) {
        res.push(postComment);
      }
    }

    return res;
  };

  const comments = useComments();

  const dispatch = useDispatch<AppDispatch>();

  if (!user) {
    return <div>Loading...</div>;
  }

  const handleUpdate = async (user: UserEntity) => {
    const frame = document.getElementById('update-user-form') as HTMLElement;
    const form = document.getElementById('input-form') as HTMLElement;

    const firstNameInput = document.getElementById(
      'input-firstname'
    ) as HTMLInputElement;
    const lastNameInput = document.getElementById(
      'input-lastname'
    ) as HTMLInputElement;
    const closer = document.getElementById('closer') as HTMLElement;
    const submitButton = document.getElementById('submit') as HTMLElement;

    toggleClass(frame, 'active');
    toggleClass(form, 'active');

    closer.onclick = (_) => {
      toggleClass(frame, 'active');
      toggleClass(form, 'active');
    };

    submitButton.onclick = (_) => {
      toggleClass(frame, 'active');
      toggleClass(form, 'active');
    };

    firstNameInput.value = user.firstName ? user.firstName : '';
    lastNameInput.value = user.lastName ? user.lastName : '';
  };

  const handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>,
    user: UserEntity
  ) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    const formProps = Object.fromEntries(formData) as updateUserDto;

    const res = await dispatch(updateUser({ ...user, ...formProps })).unwrap();
  };

  const handleDelete = async (userId: number) => {
    comments.map(
      async (comment) => await dispatch(deleteComment(comment.commentId))
    );
    posts.map(async (post) => await dispatch(deletePost(post.postId)));
    await dispatch(deleteUser(userId)).unwrap();
    localStorage.clear();
    window.location.assign('/');
  };

  return (
    <div key={user.userId} className='User'>
      <div className='update-user-form' id='update-user-form'>
        <form
          className='input-form'
          id='input-form'
          onSubmit={async (event) => {
            await handleSubmit(event, user);
          }}
        >
          <h2 className='form-title'>
            Изменить пользователя{' '}
            <span className='closer' id='closer'>
              X
            </span>
          </h2>
          <input
            placeholder='Имя'
            id='input-firstname'
            name='firstName'
          ></input>
          <input
            placeholder='Фамилия'
            id='input-lastname'
            name='lastName'
          ></input>
          <input
            placeholder='Пароль'
            type='password'
            id='password'
            name='password'
            required
          ></input>
          <input
            type='submit'
            id='submit'
            className='submit'
            value='Сохранить'
          ></input>
        </form>
      </div>
      <div className='user-header'>
        <h2>
          Пользователь {user.username} <br />
        </h2>
        {user.userId.toString() === localStorage.getItem('userId') ? (
          <div className='user-change'>
            <a
              href='#'
              onClick={() => {
                handleUpdate(user);
              }}
            >
              Изменить
            </a>
            <a
              href='#'
              onClick={() => {
                handleDelete(userId);
              }}
            >
              Удалить
            </a>
          </div>
        ) : (
          ''
        )}
      </div>
      {user.firstName ? (
        <span className='user-property'>
          <b>Имя пользователя:</b> {user.firstName}
          <br />
        </span>
      ) : (
        ''
      )}
      {user.lastName ? (
        <span className='user-property'>
          <b>Фамилия пользователя:</b> {user.lastName}
          <br />
        </span>
      ) : (
        ''
      )}
      <span className='user-property'>
        <b>Зарегистрирован с</b>{' '}
        {new Date(user.createdAt).toLocaleDateString('ru')}
      </span>
      <br />
      <h2 className='user-posts-header'>Посты {user.username}:</h2>
      <UserPostsList userId={user.userId} />
    </div>
  );
}
