import {
  createAsyncThunk,
  createSlice,
  SerializedError,
} from '@reduxjs/toolkit';
import { API_URL, AppRoutes } from '../../api';
import { signInDto, signUpDto, UserEntity } from '../../types/types';
import { RootState } from '../../app/store';

export type usersState = {
  users: UserEntity[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: SerializedError;
};

const initialState: usersState = {
  users: [] as UserEntity[],
  status: 'idle',
  error: {},
};

export const fetchUsers = createAsyncThunk('users/fetchUsers', async () => {
  const response = await fetch(API_URL + AppRoutes.USERS_BASE, {
    method: 'GET',
  });
  if (response.status >= 200 && response.status < 300) {
    return response.json();
  } else {
    let error = new Error(response.statusText);
    throw error;
  }
});

export const signUp = createAsyncThunk(
  'users/signUp',
  async (dto: signUpDto) => {
    const response = await fetch(API_URL + AppRoutes.AUTH + AppRoutes.SIGN_UP, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: dto.username,
        password: dto.password,
        firstName: dto.firstName === '' ? null : dto.firstName,
        lastName: dto.lastName === '' ? null : dto.lastName,
      }),
    });
    if (
      (response.status >= 200 && response.status < 300) ||
      response.status === 403
    ) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const signIn = createAsyncThunk(
  'users/signIn',
  async (dto: signInDto) => {
    const response = await fetch(API_URL + AppRoutes.AUTH + AppRoutes.SIGN_IN, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(dto),
    });
    if (
      (response.status >= 200 && response.status < 300) ||
      response.status === 403
    ) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const updateUser = createAsyncThunk(
  'users/updateUser',
  async (dto: UserEntity) => {
    const response = await fetch(API_URL + AppRoutes.UserEntity(dto.userId), {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
      body: JSON.stringify(dto),
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const deleteUser = createAsyncThunk(
  'users/deleteUser',
  async (id: number) => {
    const response = await fetch(API_URL + AppRoutes.UserEntity(id), {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchUsers.pending, (state, action) => {
        state.status = 'loading';
      })
      .addCase(fetchUsers.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.users = [...state.users, ...action.payload];
      })
      .addCase(fetchUsers.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error;
        console.log('state.error :>> ', state.error);
      })
      .addCase(updateUser.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.users = state.users.map((user) => {
          if (user.userId === action.payload.userId) {
            user = action.payload;
          }
          return user;
        });
      })
      .addCase(deleteUser.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.users = state.users.filter(
          (user) => user.userId !== action.payload.userId
        );
      })
      .addCase(signUp.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.users.push(action.payload);
      })
      .addCase(signIn.fulfilled, (state, action) => {
        state.status = 'succeeded';
      });
  },
});

export const selectAllUsers = (state: RootState) => state.users.users;

export const selectAllUsersSortedById = (state: RootState) =>
  state.users.users.slice().sort((a, b) => (a.userId > b.userId ? 1 : -1));

export const getUsersStatus = (state: RootState) => state.users.status;
export const getUsersError = (state: RootState) => state.users.error;

export const selectUserById = (state: RootState, userId: number) => {
  return state.users.users.find((user) => user.userId === userId);
};

export const usersActions = usersSlice.actions;

export const usersReducer = usersSlice.reducer;
