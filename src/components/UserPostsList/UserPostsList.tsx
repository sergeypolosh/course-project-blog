import { useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import { PostEntity } from '../../types/types';
import { selectUserPosts } from '../Post/postsSlice';
import './UserPostsList.css';

const renderPosts = (posts: PostEntity[]) =>
  posts.map((post) => (
    <div key={post.postId} className='SinglePost'>
      {}
      <div className='post-header'>
        <div>
          <span className='date'>
            {new Date(post.createdAt).toLocaleString('ru')}
          </span>
        </div>
        <h2>
          <a href={`/posts/${post.postId}`}>Пост #{post.postId}</a>
        </h2>
      </div>
      <div className='content'>
        <p>{post.content}</p>
        {post.createdAt === post.updatedAt ? (
          ''
        ) : (
          <div className='date'>
            Изменено: {new Date(post.updatedAt).toLocaleString('ru')}
          </div>
        )}
      </div>
    </div>
  ));

type Props = {
  userId: number;
};

export function UserPostsList({ userId }: Props) {
  const posts = useSelector((state: RootState) =>
    selectUserPosts(state, userId)
  );

  if (posts.length === 0) {
    return <div className='no-posts'>Посты отсутствуют!</div>;
  }

  return <div className='UserPosts'>{renderPosts(posts)}</div>;
}
