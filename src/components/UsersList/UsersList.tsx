import { useSelector } from 'react-redux';
import { AppRoutes } from '../../api';
import { RootState } from '../../app/store';
import { UserEntity } from '../../types/types';
import { selectAllUsersSortedById } from '../User/usersSlice';
import './UsersList.css';

const renderUsers = (users: UserEntity[]) =>
  users.map((user) => (
    <a
      href={AppRoutes.USERS_BASE + '/' + user.userId.toString()}
      key={user.userId}
    >
      <div className='User'>
        <span className='id'>#{user.userId}</span>
        <span>{user.username}</span>
        <span>{user.firstName}</span>
        <span>{user.lastName}</span>
        <span className='date'>
          {new Date(user.createdAt).toLocaleDateString('ru')}
        </span>
        <span className='date'>
          {new Date(user.updatedAt).toLocaleDateString('ru')}
        </span>
      </div>
    </a>
  ));

export function UsersList() {
  const users = useSelector((state: RootState) =>
    selectAllUsersSortedById(state)
  );

  if (users.length === 0) {
    return <div className='loading'>Loading...</div>;
  }

  return (
    <div>
      <h2>Пользователи сервиса</h2>
      <div className='UsersList'>
        <div className='header'>
          <span className='id'>ID</span>
          <span>Никнейм</span>
          <span>Имя</span>
          <span>Фамилия</span>
          <span className='date'>Дата создания</span>
          <span className='date'>Дата изменения</span>
        </div>
        {renderUsers(users)}
      </div>
    </div>
  );
}
