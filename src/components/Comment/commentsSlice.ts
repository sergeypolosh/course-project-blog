import {
  createAsyncThunk,
  createSlice,
  SerializedError,
} from '@reduxjs/toolkit';
import { API_URL, AppRoutes } from '../../api';
import { CommentEntity, createCommentDto, PostEntity } from '../../types/types';
import { RootState } from '../../app/store';

export type commentsState = {
  comments: CommentEntity[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: SerializedError;
};

const initialState: commentsState = {
  comments: [] as CommentEntity[],
  status: 'idle',
  error: {},
};

export const fetchComments = createAsyncThunk(
  'comments/fetchComments',
  async () => {
    const response = await fetch(API_URL + AppRoutes.COMMENTS_BASE, {
      method: 'GET',
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const addNewComment = createAsyncThunk(
  'comments/addNewComment',
  async (dto: createCommentDto) => {
    const response = await fetch(API_URL + AppRoutes.COMMENTS_BASE, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
      body: JSON.stringify(dto),
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const updateComment = createAsyncThunk(
  'comments/updateComment',
  async (dto: CommentEntity) => {
    const response = await fetch(
      API_URL + AppRoutes.CommentEntity(dto.commentId),
      {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
        },
        body: JSON.stringify(dto),
      }
    );
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const deleteComment = createAsyncThunk(
  'comments/deleteComment',
  async (id: number) => {
    const response = await fetch(API_URL + AppRoutes.CommentEntity(id), {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
    });
    if (response.status >= 200 && response.status < 300) {
      return { commentId: id };
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

const commentsSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchComments.pending, (state, action) => {
        state.status = 'loading';
      })
      .addCase(fetchComments.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.comments = [...state.comments, ...action.payload];
      })
      .addCase(fetchComments.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error;
        console.log('state.error :>> ', state.error);
      })
      .addCase(addNewComment.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.comments.push(action.payload);
      })
      .addCase(updateComment.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.comments = state.comments.map((comment) => {
          if (comment.commentId === action.payload.commentId) {
            comment = action.payload;
          }
          return comment;
        });
      })
      .addCase(deleteComment.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.comments = state.comments.filter(
          (comment) => comment.commentId !== action.payload.commentId
        );
        console.log('state.comments :>> ', state.comments);
      });
  },
});

export const selectAllComments = (state: RootState) => state.comments.comments;
export const getCommentsStatus = (state: RootState) => state.comments.status;
export const getCommentsError = (state: RootState) => state.comments.error;

export const selectCommentById = (state: RootState, commentId: number) =>
  state.comments.comments.find((comment) => comment.commentId === commentId);

export const selectPostComments = (state: RootState, postId: number) =>
  state.comments.comments.filter((comment) => comment.postId === postId);

export const selectUserComments = (state: RootState, userId: number) =>
  state.comments.comments.filter((comment) => comment.authorId === userId);

export const selectUserPostsComments = (
  state: RootState,
  posts: PostEntity[],
  userId: number
) => {
  const res: CommentEntity[] = [];
  for (const post of posts) {
    res.concat(
      state.comments.comments.filter(
        (comment) => comment.postId === post.postId && post.authorId === userId
      )
    );
  }

  return res;
};

export const commentsActions = commentsSlice.actions;

export const commentsReducer = commentsSlice.reducer;
