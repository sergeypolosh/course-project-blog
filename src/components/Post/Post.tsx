import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { AppDispatch, RootState } from '../../app/store';
import { PostEntity, updatePostDto } from '../../types/types';
import { deleteComment, selectPostComments } from '../Comment/commentsSlice';
import { CommentsList } from '../CommentsList';
import { selectUserById } from '../User/usersSlice';
import './Post.css';
import { deletePost, selectPostById, updatePost } from './postsSlice';

const toggleClass = (elem: HTMLElement, className: string) => {
  elem.className = elem.className.includes(className)
    ? elem.className.replace(className, '')
    : elem.className.concat(' ' + className);
};

export function Post() {
  const { id } = useParams();
  const postId = parseInt(id as string);

  const post = useSelector((state: RootState) => selectPostById(state, postId));

  const author = useSelector((state: RootState) =>
    post ? selectUserById(state, post.authorId) : undefined
  );

  const comments = useSelector((state: RootState) =>
    selectPostComments(state, postId)
  );

  const dispatch = useDispatch<AppDispatch>();

  if (!post) {
    return <div className='loading'>Loading...</div>;
  }

  if (!author) {
    return <div>Непредвиденная ошибка: у поста отсутствует автор...</div>;
  }

  const handleUpdate = async (post: PostEntity) => {
    const frame = document.getElementById('update-post-form') as HTMLElement;
    const form = document.getElementById('input-form') as HTMLElement;

    const contentInput = document.getElementById(
      'input-content'
    ) as HTMLInputElement;
    const closer = document.getElementById('closer') as HTMLElement;
    const submitButton = document.getElementById('submit') as HTMLElement;

    toggleClass(frame, 'active');
    toggleClass(form, 'active');

    closer.onclick = (_) => {
      toggleClass(frame, 'active');
      toggleClass(form, 'active');
    };

    submitButton.onclick = (_) => {
      toggleClass(frame, 'active');
      toggleClass(form, 'active');
    };

    contentInput.value = post.content ? post.content : '';
  };

  const handleSubmit = async (
    event: React.FormEvent<HTMLFormElement>,
    post: PostEntity
  ) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    const formProps = Object.fromEntries(formData) as updatePostDto;

    await dispatch(updatePost({ ...post, ...formProps })).unwrap();
  };

  const handleDelete = async (postId: number) => {
    comments.map(
      async (comment) => await dispatch(deleteComment(comment.commentId))
    );
    await dispatch(deletePost(postId)).unwrap();
    window.location.assign('/');
  };

  return (
    <div className='SinglePost'>
      <div className='update-post-form' id='update-post-form'>
        <form
          className='input-form'
          id='input-form'
          onSubmit={async (event) => {
            await handleSubmit(event, post);
          }}
        >
          <h2 className='form-title'>
            Изменить содержимое поста
            <span className='closer' id='closer'>
              X
            </span>
          </h2>
          <textarea
            placeholder='Содержимое'
            id='input-content'
            className='input-content'
            name='content'
            cols={50}
            rows={10}
            wrap='soft'
          ></textarea>
          <input
            type='submit'
            id='submit'
            className='submit'
            value='Сохранить'
          ></input>
        </form>
      </div>
      <div className='post-header'>
        <div>
          <div className='username'>
            <a href={`/users/${author.userId}`}>{author.username}</a>
          </div>
          <span className='date'>
            {new Date(post.createdAt).toLocaleString('ru')}
          </span>
        </div>
        <div className='service'>
          <h2 className='post-name'>Пост #{post.postId}</h2>
          {post.authorId.toString() === localStorage.getItem('userId') ? (
            <div className='post-change'>
              <a
                href='#'
                onClick={() => {
                  handleUpdate(post);
                }}
              >
                Изменить
              </a>
              <a
                href='#'
                onClick={() => {
                  handleDelete(postId);
                }}
              >
                Удалить
              </a>
            </div>
          ) : (
            ''
          )}
        </div>
      </div>
      <div className='content'>
        <p>{post.content}</p>
        {post.createdAt === post.updatedAt ? (
          ''
        ) : (
          <div className='date'>
            Изменено: {new Date(post.updatedAt).toLocaleString('ru')}
          </div>
        )}
      </div>
      <div>
        <CommentsList />
      </div>
    </div>
  );
}
