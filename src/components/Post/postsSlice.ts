import {
  createAsyncThunk,
  createSlice,
  SerializedError,
} from '@reduxjs/toolkit';
import { API_URL, AppRoutes } from '../../api';
import { PostEntity, createPostDto } from '../../types/types';
import { RootState } from '../../app/store';

export type postsState = {
  posts: PostEntity[];
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error: SerializedError;
};

const initialState: postsState = {
  posts: [] as PostEntity[],
  status: 'idle',
  error: {},
};

export const fetchPosts = createAsyncThunk('posts/fetchPosts', async () => {
  const response = await fetch(API_URL + AppRoutes.POSTS_BASE, {
    method: 'GET',
  });
  if (response.status >= 200 && response.status < 300) {
    return response.json();
  } else {
    let error = new Error(response.statusText);
    throw error;
  }
});

export const addNewPost = createAsyncThunk(
  'posts/addNewPost',
  async (dto: createPostDto) => {
    const response = await fetch(API_URL + AppRoutes.POSTS_BASE, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
      body: JSON.stringify(dto),
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const updatePost = createAsyncThunk(
  'posts/updatePost',
  async (dto: PostEntity) => {
    const response = await fetch(API_URL + AppRoutes.PostEntity(dto.postId), {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
      body: JSON.stringify(dto),
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

export const deletePost = createAsyncThunk(
  'posts/deletePost',
  async (id: number) => {
    const response = await fetch(API_URL + AppRoutes.PostEntity(id), {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwtToken'),
      },
    });
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    } else {
      let error = new Error(response.statusText);
      throw error;
    }
  }
);

const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchPosts.pending, (state, action) => {
        state.status = 'loading';
        console.log('state.status :>> ', state.status);
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.posts = [...state.posts, ...action.payload];
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.status = 'failed';
        state.error = action.error;
        console.log('state.error :>> ', state.error);
      })
      .addCase(addNewPost.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.posts.push(action.payload);
      })
      .addCase(updatePost.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.posts = state.posts.map((post) => {
          if (post.postId === action.payload.postId) {
            post = action.payload;
          }
          return post;
        });
      })
      .addCase(deletePost.fulfilled, (state, action) => {
        state.status = 'succeeded';
        state.posts = state.posts.filter(
          (post) => post.postId !== action.payload.postId
        );
      });
  },
});

export const selectAllPosts = (state: RootState) => state.posts.posts;

export const selectAllPostsSortedById = (state: RootState) =>
  state.posts.posts.slice().sort((a, b) => (a.postId > b.postId ? 1 : -1));

export const selectAllPostsSortedByDate = (state: RootState) =>
  state.posts.posts
    .slice()
    .sort((a, b) =>
      new Date(a.createdAt).valueOf() < new Date(b.createdAt).valueOf() ? 1 : -1
    );

export const getPostsStatus = (state: RootState) => state.posts.status;
export const getPostsError = (state: RootState) => state.posts.error;

export const selectPostById = (state: RootState, postId: number) =>
  state.posts.posts.find((post) => post.postId === postId);

export const selectUserPosts = (state: RootState, userId: number) =>
  state.posts.posts.filter((post) => post.authorId === userId);

export const postsActions = postsSlice.actions;

export const postsReducer = postsSlice.reducer;
