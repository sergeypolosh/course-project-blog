import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch } from '../../app/store';
import { createPostDto, PostEntity, UserEntity } from '../../types/types';
import { addNewPost, selectAllPostsSortedByDate } from '../Post/postsSlice';
import { selectAllUsers } from '../User/usersSlice';
import './Home.css';

const findAuthor = (users: UserEntity[], post: PostEntity) => {
  const user = users.find((user) => user.userId === post.authorId);
  return user;
};

const renderPosts = (posts: PostEntity[], users: UserEntity[]) =>
  posts.map((post) => (
    <div key={post.postId} className='SinglePost'>
      {}
      <div className='post-header'>
        <div>
          <div className='username'>
            <a href={`/users/${post.authorId}`}>
              {findAuthor(users, post)?.username}
            </a>
          </div>
          <span className='date'>
            {new Date(post.createdAt).toLocaleString('ru')}
          </span>
        </div>
        <h2>
          <a href={`/posts/${post.postId}`}>Пост #{post.postId}</a>
        </h2>
      </div>
      <div className='content'>
        <p>{post.content}</p>
        {post.createdAt === post.updatedAt ? (
          ''
        ) : (
          <div className='date'>
            Изменено: {new Date(post.updatedAt).toLocaleString('ru')}
          </div>
        )}
      </div>
    </div>
  ));

export function Home() {
  const posts = useSelector(selectAllPostsSortedByDate);
  const users = useSelector(selectAllUsers);

  const dispatch = useDispatch<AppDispatch>();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    const formProps = Object.fromEntries(formData) as createPostDto;

    const button = document.getElementById('input-submit') as HTMLInputElement;
    const originalValue = button.value;

    const textarea = document.getElementById(
      'input-field'
    ) as HTMLTextAreaElement;

    if (!textarea.value) {
      return;
    }

    try {
      const res = await dispatch(addNewPost(formProps)).unwrap();
      button.value = 'Отправлено!';
      setTimeout(() => (button.value = originalValue), 3000);
      console.log('result :>> ', res);

      textarea.value = '';
    } catch (err) {
      button.value = 'Авторизуйтесь, чтобы отправить пост!';
      setTimeout(() => (button.value = originalValue), 3000);
    }
  };

  return (
    <div className='Home'>
      <div className='create-post'>
        <form
          onSubmit={async (event) => {
            await handleSubmit(event);
          }}
        >
          <textarea
            name='content'
            id='input-field'
            className='input-field'
            cols={30}
            rows={10}
            wrap='soft'
            placeholder='Напишите что-нибудь!'
          ></textarea>
          <input id='input-submit' type='submit' value='Отправить' />
        </form>
      </div>
      {posts.length > 0 && users.length > 0 ? (
        <div className='posts'>
          <h2>Посты</h2>
          {renderPosts(posts, users)}
        </div>
      ) : (
        <div className='loading'>Loading...</div>
      )}
    </div>
  );
}
