export const API_URL: string = 'https://course-project-blog.herokuapp.com';

export const AppRoutes = {
  HOME: '/',
  USERS_BASE: '/users',
  UserEntity: (userId: number) => `/users/${userId}`,
  POSTS_BASE: '/posts',
  PostEntity: (postId: number) => `/posts/${postId}`,
  USER_POSTS: (userId: number) => `/users/${userId}/posts`,
  COMMENTS_BASE: '/comments',
  CommentEntity: (commentId: number) => `/comments/${commentId}`,
  USER_COMMENTS: (userId: number) => `/users/${userId}/comments`,
  POST_COMMENTS: (postId: number) => `/posts/${postId}/comments`,
  AUTH: '/auth',
  SIGN_UP: '/sign-up',
  SIGN_IN: '/sign-in',
};
