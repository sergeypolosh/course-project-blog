export type UserEntity = {
  userId: number;
  username: string;
  firstName?: string;
  lastName?: string;
  role: string;
  createdAt: string;
  updatedAt: string;
};

export type PostEntity = {
  postId: number;
  authorId: number;
  content: string;
  createdAt: string;
  updatedAt: string;
};

export type CommentEntity = {
  commentId: number;
  postId: number;
  authorId: number;
  content: string;
  createdAt: string;
  updatedAt: string;
};

export type signInDto = {
  username: string;
  password: string;
};

export type signInResponseDto = {
  user: UserEntity;
  jwtToken: string;
};

export type signUpDto = {
  firstName?: string;
  lastName?: string;
  username: string;
  password: string;
};

export type signUpResponseDto = {
  user: UserEntity;
  jwtToken: string;
};

export type createPostDto = {
  content: string;
};

export type createPostResponseDto = {
  postId: number;
  authorId: number;
  content: string;
  createdAt: string;
  updatedAt: string;
};

export type updatePostDto = {
  content: string;
};

export type updatePostResponseDto = {
  postId: number;
  authorId: number;
  content: string;
  createdAt: string;
  updatedAt: string;
};

export type updateUserDto = {
  firstName?: string;
  lastName?: string;
  password?: string;
};

export type updateUserResponseDto = {
  userId: number;
  username: string;
  firstName?: string;
  lastName?: string;
  role: string;
  createdAt: string;
  updatedAt: string;
};

export type createCommentDto = {
  postId: number;
  content: string;
};

export type createCommentResponseDto = {
  commentId: number;
  postId: number;
  authorId: number;
  content: string;
  createdAt: string;
  updatedAt: string;
};

export type updateCommentDto = {
  content: string;
};

export type updateCommentResponseDto = {
  commentId: number;
  postId: number;
  authorId: number;
  content: string;
  createdAt: string;
  updatedAt: string;
};
